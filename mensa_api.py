import logging
import re
from datetime import datetime

import html5lib
import requests

# --- 2017, Georg Sauthoff <mail@gms.tf>

xns_url = '{http://www.w3.org/1999/xhtml}'
ons = '{http://openmensa.org/open-mensa-v2}'

logger = logging.getLogger(__name__)


def get_tables(html):
    root_element = html5lib.parse(html)
    tabels = []
    for children in root_element.iter():
        if children.tag == xns_url + 'h2':
            last_h2 = children
        elif children.tag == xns_url + 'div' and children.get('class') == 'mensa plan':
            tabels.append((last_h2, children.find(xns_url + 'div').find(xns_url + 'table')))
    return tabels


# ----

def contain_additives(s):
    return re.fullmatch(r"([A-Z]\d+)+", re.sub(r"[,.]", "", s))


def parse_name(menu_elements, remove_additives=True):
    menu_texts = [y for y in (x.strip() for p in menu_elements for x in p.itertext())
                  if y and 'Details' not in
                  y and (not remove_additives or not contain_additives(y)) and 'kcal' not in y]
    if 'Dazu gibt es:' in menu_texts:
        idx = menu_texts.index('Dazu gibt es:')
        menu_texts[idx] = '- ' + menu_texts[idx]
    return " ".join(menu_texts) if menu_texts else ''


def parse_table(header, body):
    relevant_categories = ['Tagesmenü', 'Menü vegetarisch', 'Menü vegan', 'Mensa vital',
                           'Eintopf', 'Eintopf / Suppe', 'Aktions-Theke']
    date = re.search(r'\d{2}\.\d{2}\.\d{4}', header.text).group(0)
    day = datetime.strptime(date, '%d.%m.%Y').date()
    menus = []
    sides = set()
    for tr in body.find(xns_url + 'tbody').iter(xns_url + 'tr'):
        tds = tr.findall(xns_url + 'td')
        menu = ''.join(tds[0].find(xns_url + 'h3').itertext()).strip()
        if menu not in relevant_categories:
            continue
        menu = convert_category(menu)
        ls = parse_name(tds[0].findall(xns_url + 'p'))
        prices_string = ' '.join(tds[1].itertext())
        price_matches = re.search(r'\d+(?:,(?:-|\d{2}))?', prices_string)
        price = price_matches.group() if price_matches else None
        if menu in ["Vegetarisch", "Tagesmenü"]:
            s = ls.split(' Es können 3 Wahlbeilagen gewählt werden:')
            ls = s[0]

            if len(s) > 1:
                # Normal menu-entry, proceed as usual
                sides.update([x.strip() for x in s[1].split(", ")])
            else:
                # Unusual entry. Mensa is closed or something due to holiday, etc.
                return None, None
        menus.append(Menu(menu, ls, price))
    menus.append(Menu('Beilagen', ", ".join(sides), None))
    return day, menus


def convert_category(category):
    if category == "Eintopf / Suppe":
        return "Suppe"
    if category == "Mensa vital":
        return "Vital"
    if category == "Aktions-Theke":
        return "Action-Theke"
    if category == "Menü vegetarisch" or category == "Menü vegan":
        return "Vegetarisch"
    return category


class Menu:
    def __init__(self, category, text, price):
        self.name = category
        self.text = text
        self.price = price

    def __repr__(self) -> str:
        return self.name

    def __eq__(self, other):
        return self.name == other.name and self.text == other.text and self.price == other.price

    def __str__(self):
        if self.price:
            return ("👉 *{meal.name}*: {meal.text} _({meal.price} €)_\n"
                    .format(meal=self))
        return "👉 *{meal.name}*: {meal.text}\n".format(meal=self)


def get():
    url = 'http://www.studierendenwerk-bielefeld.de/essen-trinken/essen-und-trinken-in-mensen/bielefeld/mensa-gebaeude-x.html'
    agent = 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/59.0.3071.115 Safari/537.36'
    session = requests.Session()
    session.headers.update({'User-Agent': agent})
    response = session.get(url)
    response.raise_for_status()
    return response.text


def parse_week(html):
    tables = get_tables(html)
    days = dict()
    for h2, tbl in tables:
        day, menus = parse_table(h2, tbl)
        days[day] = menus
    return days


def get_menu(day=None):
    if not day:
        day = datetime.today().date()

    html_response = get()

    week = parse_week(html_response)
    if day not in week:
        logger.info("Failed to get menu for {}".format(day))
        weekday = day.weekday()
        if weekday == 5:
            return "Heute keine Mensa. Es ist Samstag. 🤦"
        if weekday == 6:
            return "Heute keine Mensa. Es ist Sonntag. 🤦"
        return "Für heute kann ich keinen Speiseplan finden. 🤔 Die Mensa hat wahrscheinlich zu..."

    menus = week[day]

    d = {"Tagesmenü": "", "Vegetarisch": "", "Beilagen": "",
         "Vital": "", "Eintopf": "", "Suppe": "", "Action-Theke": ""}
    for m in menus:
        if m.name in d:
            d[m.name] += str(m)

    m = ("{Tagesmenü}"
         "{Vegetarisch}"
         "{Beilagen}"
         "{Vital}"
         "{Eintopf}"
         "{Suppe}"
         "{Action-Theke}").format(**d)
    return m
