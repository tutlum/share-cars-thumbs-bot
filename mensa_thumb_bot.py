import logging
import sys

from telegram import ParseMode, InlineKeyboardButton, InlineKeyboardMarkup
from telegram.ext import CommandHandler, InlineQueryHandler, CallbackQueryHandler
from telegram.ext import MessageHandler, Filters
from telegram.ext import Updater

import schedule
from chat import Chat
from mensa_api import get_menu

logger = logging.getLogger(__name__)

# module variables
chats = dict()
mensa_bot = None


def get_chat(chat_id):
    if chat_id not in chats:
        chats[chat_id] = Chat(chat_id)
    return chats[chat_id]


# bot functions
def handle_time_queries(_, update):
    if update.message:
        query = update.message.text
        from_user = update.message.from_user
        chat_id = update.message.chat_id
    elif update.callback_query:
        query = update.callback_query.data
        from_user = update.callback_query.from_user
        chat_id = update.callback_query.message.chat_id
    else:
        raise ValueError("no message or callbackquery in update")
    chat = get_chat(chat_id)
    if not chat.validate_and_process_time_query(query, from_user, send_reminder):
        return
    times = chat.times()
    keyboard = [
        [InlineKeyboardButton("{:%H:%M} 👍".format(t), callback_data=t.strftime('%H:%M')) for t in sorted(times)]]
    reply_markup = InlineKeyboardMarkup(keyboard)
    if chat.status_message:
        mensa_bot.edit_message_text(text=chat.generate_status(),
                                    chat_id=chat_id, message_id=chat.status_message.message_id,
                                    reply_markup=reply_markup, parse_mode=ParseMode.MARKDOWN)
    else:
        chat.status_message = send_message(chat_id, chat.generate_status(), reply_markup=reply_markup)
        pin_message(chat_id, chat.status_message)
        schedule.schedule_cleaning(unpin_message, chat)


def send_reminder(chat, time_slot):
    send_message(chat.chat_id, chat.reminder(time_slot))


def send_message(chat_id, message, parse_mode=ParseMode.MARKDOWN, reply_markup=None):
    if callable(message):
        message = message()
    if message:
        return mensa_bot.send_message(chat_id=chat_id, text=message, parse_mode=parse_mode, reply_markup=reply_markup)


def pin_message(chat_id, message):
    return mensa_bot.pin_chat_message(chat_id, message.message_id, True)


def unpin_message(chat):
    return mensa_bot.unpin_chat_message(chat.chat_id)


def status(_, update):
    chat_id = update.message.chat_id
    send_message(chat_id, get_chat(chat_id).generate_status())


def menu(_, update):
    logger.info("Menu requested. Sending message with menu for today")
    send_message(update.message.chat_id, get_menu())


def main():
    if len(sys.argv) < 2:
        print("Usage: python mensa-thumb-bot.py <TELEGRAM-API-KEY>")
        return

    logging.basicConfig(
        format='%(asctime)s - %(name)s - %(levelname)s - %(message)s',
        level=logging.INFO)

    updater = Updater(token=sys.argv[1])
    dispatcher = updater.dispatcher
    global mensa_bot
    mensa_bot = updater.bot

    dispatcher.add_handler(MessageHandler(Filters.text, handle_time_queries))
    dispatcher.add_handler(CommandHandler('menu', menu, filters=Filters.private))
    dispatcher.add_handler(CallbackQueryHandler(handle_time_queries))

    updater.start_polling()
    logger.info("Bot started successfully and is now listening. :)")
    schedule.scheduler.start()


if __name__ == "__main__":
    main()
