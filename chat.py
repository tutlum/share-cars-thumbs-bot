import logging
import re
from datetime import datetime, timedelta
from collections import OrderedDict
from datetime import time
from itertools import count, starmap

import schedule

logger = logging.getLogger(__name__)


class Chat:
    def __init__(self, chat_id) -> None:
        self.user_ref = dict()
        self.chat_id = chat_id
        self.users_to_time = dict()
        self.activeSuggestion = None
        self.status_message = None

    def times(self):
        return set(self.users_to_time.values())

    def get_time_dict(self):
        time_dict = OrderedDict(zip(sorted(self.users_to_time.values()), (list() for _ in count())))
        for k, v in self.users_to_time.items():
            time_dict[v].append(self.user_ref[k])
        return time_dict

    def remove_user(self, user):
        if user.id in self.users_to_time:
            del self.users_to_time[user.id]
            del self.user_ref[user.id]

    def thumb_up(self, user, time_slot=None):
        if not time_slot and not self.activeSuggestion:
            return
        time_slot = time_slot if time_slot else self.activeSuggestion
        self.user_ref[user.id] = user
        self.users_to_time[user.id] = time_slot
        self.activeSuggestion = time_slot

    def generate_status(self):
        time_dict = self.get_time_dict()
        if len(time_dict) == 0:
            return ("Bislang will wohl niemand essen gehen."
                    "🤔 Du kannst jedoch einfach eine Zeit vorschlagen!\n"
                    "*Beispiel:* _1?_")
        time_slots = "\n".join(filter(None.__ne__, starmap(timeslot_to_string, time_dict.items())))
        return ("*Bislang wollen mit:*\n\n"
                "{}\n\n"
                "Ich schicke diese Nachricht aber noch kurz vorher rum. 😉"
                ).format(time_slots)

    def validate_and_process_time_query(self, query, user, reminder_callback=None):
        query = re.sub(r"[🏻🏼🏽🏾🏿]", "", query)
        username = user.first_name.replace('_', '\\_')

        old_times = self.times()
        old_dict = self.users_to_time.copy()

        if query == '👍':
            self.thumb_up(user)
            logger.info("THUMB! Will add {} to {} ...".format(username, self.activeSuggestion))

        elif '👎' in query:
            if '👍' in query:
                logger.info("{} cant't decide *faceplam*".format(user))
                return
            self.remove_user(user)

        else:
            query = re.sub(r"[👍?.!]$", "", query)  # remove everything unnecessary
            time_slot = parse_time(query)
            if not time_slot or not time(11, 30) <= time_slot <= time(14, 30):
                return
            logger.info(
                "Successfully parsed {}. Will add {} to {:%H:%M} ...".format(
                    query, username, time_slot))
            self.thumb_up(user, time_slot)

        # update if times changed
        times = self.times()
        if times != old_times:
            new_times = times - old_times
            removed_times = old_times - times
            # sanity checks
            if len(removed_times) > 1:
                raise ValueError("removed_times > 1, this shouldn't be possible!!")
            if len(new_times) > 1:
                raise ValueError("new_times > 1, this shouldn't be possible")

            if len(removed_times) == 1:
                removed_time = removed_times.pop()
                schedule.remove_job(removed_time)

            if len(new_times) == 1:
                new_time = new_times.pop()
                if reminder_callback:
                    schedule.add_reminder(new_time, reminder_callback, self)
        return old_dict != self.users_to_time

    def reminder(self, time_slot):
        return ("*Bislang wollen mit:*\n\n"
                "{}\n\n"
                "*{:%H:%M} Uhr-Gruppe, Zeit zum Aufbrechen! 🚶‍♀️🚶*"
                ).format(timeslot_to_string(time_slot, self.get_time_dict()[time_slot]), time_slot)


def timeslot_to_string(timeslot, attendants):
    if len(attendants) <= 0:
        return None
    attendants = ["{first_name}{initial}"
                  .format(first_name=user.first_name, initial=(" {last_name[0]}.".format(last_name=user.last_name) if user.last_name else ""))
                  .replace('_', '\\_')
                  for user in attendants]
    output = "*Um {:%H:%M} Uhr:*\n{}".format(timeslot, ", ".join(attendants))
    if len(attendants) > 1:
        output += " _({} Leute)_".format(len(attendants))
    return output


def custom_round(x):
    round_value = 15
    fdiv = x // round_value
    if x % round_value >= round_value // 2:
        fdiv += 1
    return fdiv * round_value


def parse_time(t):
    def round_time(s):
        if not s:
            return
        return s.replace(minute=custom_round(s.minute))

    def parse(s):
        s = s.strip()
        offset = timedelta(0)
        try:
            if len(s) == 5:
                try:
                    return datetime.strptime(s, '%H:%M').time()
                except ValueError:
                    return datetime.strptime(s, '%H.%M').time()
            if s.startswith('halb '):
                offset = timedelta(minutes=30)
                s = s.lstrip('halb ')
            if len(s) == 1:
                return (datetime.strptime(s, '%H') + timedelta(
                    hours=12) - offset).time()
            if len(s) == 2:
                return (datetime.strptime(s, '%H') - offset).time()
        except ValueError:
            return

    return round_time(parse(t))
