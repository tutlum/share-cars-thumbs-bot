result=$(grep -U $'\x0D' *.py -lr)

if [ "$result" ]; then
    echo "Found CRLF line breaks in the following files:"
    echo $result
    exit 1
fi
